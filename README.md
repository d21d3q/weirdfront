# weirdfront

Frontend for [d21d3q/weirdapi](https://gitlab.com/d21d3q/weirdapi/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
Running in dev mode will proxy api calls to `localhost:8080` (weirdapi)

### Compiles and minifies for production
```
npm run build
```
# Deploymen

Deploy with traefik, just like [weirdapi](https://gitlab.com/d21d3q/weirdapi/)
