import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios'
import axios from 'axios'

import App from './App.vue'
import Home from './components/Home.vue'
import Encode from './components/Encode.vue'
import Decode from './components/Decode.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VueAxios, axios)

const routes = [
  { path: '/', component: Home },
  { path: '/encode', component: Encode },
  { path: '/decode', component: Decode }
]

const router = new VueRouter({
  routes
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
